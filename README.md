# 24g Assessment Project

Steven Olthoff

![Image](public/eg.png)

## Setup

### Setup Back-End:

First, set up the back-end. This involves a single command. The database and schema will be automatically created.

- `git clone git@gitlab.com:stevenolthoff/24g-back.git`

- `docker-compose up`

### Setup Front-End

Now setup the Vue project as normal.

- `npm i`

- `npm run serve`

- Open `http://localhost:8080/`
