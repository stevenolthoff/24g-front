import Vue from "vue"
import VueRouter, { RouteConfig } from "vue-router"
import Videos from "../views/videos/Videos.vue"

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: "/videos/:id?",
    name: "Videos",
    component: Videos,
  },
  {
    path: "*",
    component: Videos,
  },
]

const router = new VueRouter({
  routes,
  mode: "history",
})

export default router
