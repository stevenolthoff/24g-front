import axios from "axios"
import Video from "./interfaces/Video"
import Rating from "./interfaces/Rating"
import UserVideoRating from "./interfaces/UserVideoRating"
import Comment from "./interfaces/Comment"

interface RatingCounts {
  thumbs_up: number
  thumbs_down: number
}

export default class Api {
  static async getVideos() {
    const response = await axios({
      url: `${process.env.VUE_APP_API}/videos`,
    })
    const videosResponse: { videos: Video[] } = response.data
    return videosResponse
  }
  static async getVideo(id: number) {
    const response = await axios({
      url: `${process.env.VUE_APP_API}/videos/${id}`,
      params: {
        user_id: 1,
      },
    })
    const videoResponse: { video: Video } = response.data
    return videoResponse
  }
  static async getVideoRating(id: number) {
    const response = await axios({
      url: `${process.env.VUE_APP_API}/videos/${id}/rating`,
    })
    const ratingResponse: {
      counts: RatingCounts
    } = response.data
    return ratingResponse
  }
  static async getUserVideoRating(userId: number, videoId: number) {
    const response = await axios({
      url: `${process.env.VUE_APP_API}/users/${userId}/videos/${videoId}/rating`,
    })
    const userVideoRatingResponse: { userVideoRating: UserVideoRating } =
      response.data
    return userVideoRatingResponse
  }
  static async putUserVideoRating(
    userId: number,
    videoId: number,
    rating: "thumbs_up" | "thumbs_down"
  ) {
    const response = await axios({
      url: `${process.env.VUE_APP_API}/users/${userId}/videos/${videoId}/rating`,
      method: "put",
      data: {
        rating,
      },
    })
    const userVideoRatingResponse: {
      userVideoRating: { rating: Rating }
      counts: RatingCounts
    } = response.data
    return userVideoRatingResponse
  }
  static async deleteUserVideoRating(userId: number, videoId: number) {
    const response = await axios({
      url: `${process.env.VUE_APP_API}/users/${userId}/videos/${videoId}/rating`,
      method: "delete",
    })
    const userVideoRatingResponse: {
      userVideoRating: UserVideoRating
      counts: RatingCounts
    } = response.data
    return userVideoRatingResponse
  }
  static async getVideoComments(id: number) {
    const response = await axios({
      url: `${process.env.VUE_APP_API}/videos/${id}/comments`,
    })
    const commentsResponse: {
      comments: Comment[]
    } = response.data
    return commentsResponse
  }
  static async putVideoComments(
    userId: number,
    videoId: number,
    content: string
  ) {
    const response = await axios({
      url: `${process.env.VUE_APP_API}/users/${userId}/videos/${videoId}/comments`,
      method: "put",
      data: {
        content,
      },
    })
    const commentsResponse: {
      comment: Comment
    } = response.data
    return commentsResponse
  }
}
