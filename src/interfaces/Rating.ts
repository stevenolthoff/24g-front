enum Rating {
  thumbs_up = "thumbs_up",
  thumbs_down = "thumbs_down",
}
export default Rating
