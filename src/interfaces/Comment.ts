import User from "./User"
import Video from "./Video"

export default interface Comment {
  content: string
  user: User
  video: Video
  created_at: string
}
