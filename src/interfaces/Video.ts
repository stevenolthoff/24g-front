export default interface Video {
  id: number
  title: string
  video: string
  thumb: string
  views: number
}
