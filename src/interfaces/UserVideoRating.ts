import Rating from "./Rating"

export default interface UserVideoRating {
  rating: Rating
}
